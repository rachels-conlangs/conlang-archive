import os

replaceStrings = []
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/43", "new" : "about.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/whatsnew", "new" : "whatsnew.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/16", "new" : "laadanlessons.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/4", "new" : "laadantoenglish.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/5", "new" : "englishtolaadan.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/55", "new" : "grammarupdates.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/suzettenotes", "new" : "notes.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/33", "new" : "reference.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/blog", "new" : "blog.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/44", "new" : "corrections.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/node/38", "new" : "links.html" } )
replaceStrings.append( { "old" : "https://web.archive.org/web/20120621060536/http://www.laadanlanguage.org:80/pages/contact", "new" : "contact.html" } )
replaceStrings.append( { "old" : "/web/20120621080932/http://www.laadanlanguage.org/pages/", "new" : "index.html" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/modules/book/book.css?J", "new" : "CSS/book.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/sites/all/modules/cck/theme/content-module.css?J", "new" : "CSS/content-module.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/modules/system/defaults.css?J", "new" : "CSS/default.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/sites/all/modules/fckeditor/fckeditor.css?J", "new" : "CSS/fckeditor.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/modules/node/node.css?J", "new" : "CSS/node.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/sites/all/themes/bluemarine/style.css?J", "new" : "CSS/style.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/modules/system/system.css?J", "new" : "CSS/system.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/modules/system/system-menus.css?J", "new" : "CSS/system-menus.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/modules/user/user.css?J", "new" : "CSS/user.css" } )
replaceStrings.append( { "old" : "/web/20120621080932cs_/http://www.laadanlanguage.org/pages/sites/all/modules/views/css/views.css?J", "new" : "CSS/views.css" } )
replaceStrings.append( { "old" : "/web/20120621080932im_/http://www.laadanlanguage.org/pages/sites/default/files/bluemarine_logo.gif", "new" : "IMAGES/wreath.gif" } )

rootFolder = os.path.dirname(os.path.realpath(__file__))
os.system( "rm old_*" )

file_list = list()

def Sanitize( htmlFile, replaceStrings ):
    print( "Sanitize " + htmlFile )
    os.system( "cp " + htmlFile + " old_" + htmlFile )
    inHandle = open( "old_" + htmlFile, "r" )
    outHandle = open( htmlFile, "w" )

    for line in inHandle:
        for rs in replaceStrings:
            line = line.replace( rs["old"], rs["new"] )
        outHandle.write( line )

for root, directories, files in os.walk( ".", topdown = False ):
    for f in files:
        if ".html" in f:
            file_list.append( f )

for f in file_list:
    Sanitize( f, replaceStrings )
